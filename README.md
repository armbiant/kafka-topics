kafka-topics
============

Connects to the provided kafka pods and ensures that the given topics exist,
creating them with the given partitions and replication factor if necessary

Requirements
------------

* a working, configured `kubectl` binary on the `$PATH`

Role Variables
--------------

```yaml
kafka_topics:
- name: ec2-instances
  replication_factor: 1
  partitions: 1
# optionally:
ansible_kubectl_container: kafka
ansible_kubectl_namespace: kafka
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: kafka-0
      # since the containers do not have python in them
      gather_facts: no
      connection: kubectl
      roles:
      - role: kafka-topics
```

License
-------

Apache 2

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
